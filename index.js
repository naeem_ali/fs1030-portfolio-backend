import dotenv from "dotenv";
import express from "express";
import cors from "cors";
import userRoute from "./routes/userRoute";
import portfolio from "./routes/portfolio";
import userEntires from "./routes/userEntries";
import resume from "./routes/resume";

const app = express();
dotenv.config();
app.use(express.json()); // allows us to parse json
app.use(cors({ origin: "*" }));
const port = process.env.PORT || 8080;

// Available Routes
app.use("/user", userRoute);
app.use("/portfolio", portfolio);
app.use("/userEntries", userEntires);
app.use("/resume", resume);

app.use((err, req, res, next) => {
  if (res.headersSent) {
    return next(err);
  }
  console.log(err);
  res.status(404).send(`{"message": ${err}}`);
});

app.use("*", (req, res, next) => {
  console.log("Api not found");
  res.status(404).send(`{"message": "Not Found"}`);
});

app.listen(port, () => {
  console.log(`Server started on http://localhost:${port}`);
});

require("dotenv").config({ path: "./../.env" });
const mysql = require("mysql");

const db = mysql.createConnection({
  host: "localhost",
  user: process.env.DB_USER,
  password: process.env.DB_PASS,
  database: process.env.DB_NAME,
});

db.connect(function (err) {
  if (err) throw err;
  console.log("MySQL database is connected");

  // create tables
  let usersTable =
    "CREATE TABLE users ( id INT UNSIGNED NOT NULL AUTO_INCREMENT, username VARCHAR(255)  DEFAULT '', email VARCHAR(255) NOT NULL DEFAULT '',registered_date date DEFAULT NULL, last_login_date date DEFAULT NULL, password VARCHAR(255) NOT NULL DEFAULT '', PRIMARY KEY (id) )";
  let portfolio =
    "CREATE TABLE portfolio ( P_id INT UNSIGNED NOT NULL AUTO_INCREMENT, title VARCHAR(255)  DEFAULT '', category VARCHAR(255) NOT NULL DEFAULT '', project_date date DEFAULT NULL, image VARCHAR(900) NOT NULL DEFAULT '', description VARCHAR(900) NOT NULL DEFAULT '',PRIMARY KEY (P_id))";
  let userEntries =
    "CREATE TABLE users_entries ( U_id INT UNSIGNED NOT NULL AUTO_INCREMENT, name VARCHAR(255)  DEFAULT '', email VARCHAR(255) NOT NULL DEFAULT '', entry_date date DEFAULT NULL, message VARCHAR(900) NOT NULL DEFAULT '', PRIMARY KEY (U_id))";
  let work =
    "CREATE TABLE work (W_id INT UNSIGNED NOT NULL AUTO_INCREMENT,  company VARCHAR(255)  DEFAULT '', position VARCHAR(255) NOT NULL DEFAULT '', start_date date DEFAULT NULL, end_date date DEFAULT NULL, description VARCHAR(900) NOT NULL DEFAULT '', PRIMARY KEY (W_id))";
  let education =
    "CREATE TABLE education (E_id INT UNSIGNED NOT NULL AUTO_INCREMENT, institution VARCHAR(255)  DEFAULT '', program VARCHAR(255) NOT NULL DEFAULT '', start_date date DEFAULT NULL, end_date date DEFAULT NULL, PRIMARY KEY (E_id))";
  let skill =
    "CREATE TABLE skills (S_id INT UNSIGNED NOT NULL AUTO_INCREMENT, skill VARCHAR(255)  DEFAULT '', level VARCHAR(255) NOT NULL DEFAULT '', percentage INT NOT NULL, PRIMARY KEY (S_id))";

  // users table

  db.query(usersTable, (err, result) => {
    if (err) throw err;
    console.log("Users table created");
  });

  //portfolio table
  db.query(portfolio, (err, result) => {
    if (err) throw err;
    console.log("Portfolio table created");
  });

  //user Entry table
  db.query(userEntries, (err, result) => {
    if (err) throw err;
    console.log("User Entry table created");
  });

  //Work table
  db.query(work, (err, result) => {
    if (err) throw err;
    console.log("Work table created");
  });

  //Education table
  db.query(education, (err, result) => {
    if (err) throw err;
    console.log("Education table created");
  });

  //Skill table
  db.query(skill, (err, result) => {
    if (err) throw err;
    console.log("Skill table created");
  });
});

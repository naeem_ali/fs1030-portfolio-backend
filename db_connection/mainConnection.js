import dotenv from "dotenv";
import mysql from "mysql";

dotenv.config();

// const db = mysql.createConnection({
//   host: process.env.DB_HOST,
//   user: process.env.DB_USER,
//   password: process.env.DB_PASS,
//   database: process.env.DB_NAME,
// });

const configuration = {
  user: process.env.DATABASE_USER,
  password: process.env.DATABASE_PASSWORD,
  database: process.env.DATABASE_NAME,
};

if (process.env.DATABASE_SOCKET) {
  configuration.socketPath = process.env.DATABASE_SOCKET;
} else {
  configuration.host = process.env.DB_HOST;
}

const db = mysql.createConnection(configuration);

db.connect(function (err) {
  if (err) {
    console.log(err);
  } else {
    console.log(`MySQL database is connected`);
  }
});
export default db;

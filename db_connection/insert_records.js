require("dotenv").config({ path: "./../.env" });
const mysql = require("mysql");

const db = mysql.createConnection({
  host: "localhost",
  user: process.env.DB_USER,
  password: process.env.DB_PASS,
  database: process.env.DB_NAME,
});

db.connect(function (err) {
  if (err) throw err;
  console.log("MySQL database is connected");

  let sql = `INSERT INTO users_entries (name, email,  message) VALUE ?`;

  let value = [
    [
      "Max",
      "max@gmail.com",
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
    ],
    [
      "John",
      "john@gmail.com",
      "Dummy text of the printing and typesetting industry.",
    ],
    [
      "Ashley",
      "ashley@gmail.com",
      "Many desktop publishing packages and web page editors now use Lorem Ipsum",
    ],
    [
      "Thomas",
      "Thomas@gmail.com",
      "The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested.",
    ],
  ];

  // Insert multiple data
  db.query(sql, [value], (err, result) => {
    if (err) throw err;
    console.log("Users Entries Added");
  });

  db.query(`UPDATE users_entries SET entry_date = now()`, (err, result) => {
    if (err) throw err;
    console.log("update entry date");
  });

  const portfolioSql = `INSERT INTO portfolio(title, category, image, description, project_date)VALUE ?`;
  let PortfolioValue = [
    [
      "E-Commerce Web",
      "Web",
      "https://cdn.cyberchimps.com/wp-content/uploads/2018/05/The-Future-of-Commerce-Is-Yours-BigCommerce.jpg",
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
    ],
    [
      "Finance App",
      "Mobile",
      "https://i.pinimg.com/originals/3a/c6/2f/3ac62f1c7630a6a085b2df4a9e3631b1.png",
      "Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
    ],
    [
      "Travel",
      "Design",
      "https://elements-cover-images-0.imgix.net/c99676c2-b4ba-4c25-9018-74685242f7aa?auto=compress%2Cformat&fit=max&w=710&s=d960c20e0129d83ef3b76b1bcc4a413b",
      "Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
    ],
    [
      "Snow Ball",
      "Branding",
      "https://media.crystallize.com/snowball/20/3/4/25/branding-mockup.jpg",
      "Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
    ],
  ];

  db.query(portfolioSql, [PortfolioValue], (err, res) => {
    if (err) throw err;
    console.log("Portfolio added");
  });

  db.query(`UPDATE portfolio SET project_date = now()`, (err, result) => {
    if (err) throw err;
    console.log("project date added");
  });
});

require("dotenv").config({ path: "./../.env" });
const mysql = require("mysql");

const db = mysql.createConnection({
  host: "localhost",
  user: process.env.DB_USER,
  password: process.env.DB_PASS,
});

let db_name = process.env.DB_NAME; // Set database name in .env file first

db.connect(function (err) {
  if (err) throw err;
  console.log("MySQL database is connected");

  // create database
  db.query(`CREATE DATABASE ${db_name}`, function (err, result) {
    if (err) throw err;
    console.log("Database created");
  });
});

require("dotenv").config({ path: "./../.env" });
const mysql = require("mysql");

const db = mysql.createConnection({
  host: "localhost",
  user: process.env.DB_USER,
  password: process.env.DB_PASS,
});

db.connect(function (err) {
  if (err) {
    console.log(err);
  } else {
    console.log(`MySQL database is connected`);
  }
});

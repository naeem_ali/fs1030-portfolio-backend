import jwt from "jsonwebtoken";

const validation = (req, res, next) => {
  if (!req.body.username || req.body.username.length < 4) {
    return res.status(400).send({
      msg: "Please enter a username with min 4 character",
    });
  }

  // password min 6 chars
  if (!req.body.password || req.body.password.length < 6) {
    return res.status(400).send({
      msg: "Please enter a password with min 6 character",
    });
  }
  console.log(req.body.password);
  // password confirmation does not match
  if (
    !req.body.password_confirm ||
    req.body.password != req.body.password_confirm
  ) {
    return res.status(400).send({
      msg: "Both password must match",
    });
  }
  console.log(req.body.password_confirm);

  next();
};
//================= user validation ===========//

const isLoggedIn = (req, res, next) => {
  const authHeader = req.headers["authorization"];

  // Value of the authorization header is typically: "Bearer JWT", hence splitting with empty space and getting second part of the split
  const token = authHeader && authHeader.split(" ")[1];
  if (token == null) {
    return res.status(400).send({ message: "token not found" });
  }
  try {
    const data = jwt.verify(token, "SECRETKEY");
    req.userdata = data;
    next();
  } catch (err) {
    console.error(err);
    return res.status(401).send({ message: err.message });
  }
};

export { validation, isLoggedIn };

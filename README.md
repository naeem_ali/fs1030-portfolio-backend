# FS1040-Portfolio-Backend

This Repository is the Backend Part of my Portfolio web application. As an admin user, you can perform CRUD operations in this app to add, update and delete data. Also, utilize GCP (Cloud Run) service to deploy the working app as well as cloud-hosted databases (Cloud SQL).

### This project includes the following technologies:
1. Node.js
2. Express.js
3. MySql

## CI/CD
To run continuous integration and delivery please open the link to Gitlab 
repo [https://gitlab.com/naeem_ali/fs1030-portfolio-backend](https://gitlab.com/naeem_ali/fs1030-portfolio-backend) and go to solution branch and merge it to the main branch. CI/CD pipeline only includes build and deploy stages test stage is not included in CI/CD pipeline.

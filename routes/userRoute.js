import express from "express";
import bcrypt from "bcryptjs"; // bcrypt
import jwt from "jsonwebtoken";
const router = express.Router();
import db from "../db_connection/mainConnection"; // databases
import { validation, isLoggedIn } from "../Middelware/userValidation"; // Validation & Auth Middelware

//============================ Registration Route ===========================================//
router.post("/register", validation, (req, res) => {
  bcrypt.hash(req.body.password, 10, (err, hash) => {
    if (err) {
      return res.status(500).send({
        msg: err,
      });
    } else {
      db.query(
        `INSERT INTO users( username, email, password, registered_date)VALUE( ?, ?, ?, now() )`,
        [`${req.body.username}`, `${req.body.email}`, `${hash}`],
        (err, result) => {
          if (err) {
            return res.status(400).send({
              msg: err,
            });
          }
          return res.status(201).send({
            msg: "Registered",
          });
        }
      );
    }
  });
});

//============================ Login Route ===========================================//
router.post("/login", (req, res) => {
  const email = req.body.email;
  const password = req.body.password;

  db.query(`SELECT * FROM users WHERE email = '${email}'`, (err, result) => {
    if (err) {
      return res.status(400).send({ msg: err });
    }

    if (!result.length) {
      return res.status(401).send({
        msg: "Username or password is incorrect!",
      });
    }

    const hashpassword = result[0].password;

    bcrypt.compare(password, hashpassword, function (berr, bresult) {
      if (berr) {
        return res
          .status(401)
          .send({ msg: "Username or password is incorrect!" });
      }

      if (bresult) {
        const token = jwt.sign(
          {
            username: result[0].username,
            userId: result[0].id,
          },
          `${process.env.PRIVATE_KEY}`,
          {
            expiresIn: "1hr",
          }
        );

        db.query(
          `UPDATE users SET last_login_date = now() WHERE id = '${result[0].id}'`
        );

        return res.status(200).send({
          msg: "Logged in!",
          token,
          user: result[0],
        });
      }

      return res.status(401).json({
        msg: "Username or password is incorrect!",
      });
    });
  });
});

//============================ Edit User By Id Route ===========================================//
router.get("/", isLoggedIn, (req, res) => {
  return res.send(req.userdata);
});

//============================ Delete User By Id Route ===========================================//
router.delete("/:id", (req, res) => {});

export default router;

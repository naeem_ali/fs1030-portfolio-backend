import express from "express";
const router = express.Router();
import { validation, isLoggedIn } from "../Middelware/userValidation"; // Validation & Auth Middelware
import db from "../db_connection/mainConnection"; // databases

//============================ View All Portfolio ===========================================//

router.get("/", isLoggedIn, (req, res) => {
  db.query(`SELECT * FROM portfolio`, (err, result) => {
    if (err) {
      return res.status(400).send({ msg: err });
    }

    return res.status(200).json(result);
  });
});

//============================ Post New Portfolio Project ===========================================//

router.post("/", (req, res) => {
  db.query(
    `INSERT INTO portfolio(title, category, image, description, project_date) VALUES(?, ?, ?, ?, now());`,
    [
      `${req.body.title}`,
      `${req.body.category}`,
      `${req.body.image}`,
      `${req.body.description}`,
    ],
    (err, result) => {
      if (err) {
        return res.status(400).send({ msg: err });
      }

      return res.status(200).json({ msg: "Inserted New Portfolio" });
    }
  );
});

//============================ Get Portfolio By ID ===========================================//

router.get("/:id", isLoggedIn, (req, res) => {
  const id = req.params.id;

  db.query(`SELECT * FROM portfolio WHERE P_id = ${id}`, (err, result) => {
    if (err) {
      return res.status(400).send({ msg: err });
    }

    return res.status(200).json(result);
  });
});

//============================ Edit Portfolio By ID ===========================================//

router.put("/:id", (req, res) => {
  const id = req.params.id;
  db.query(
    `UPDATE portfolio SET P_id  = '${req.body.P_id}', title = '${req.body.title}', category = '${req.body.category}', image = '${req.body.image}',description = '${req.body.description}' WHERE P_id  = ${id};`,
    (err, result) => {
      if (err) {
        return res.status(400).send({ msg: err });
      }

      return res.status(200).json({ msg: "Data Updated" });
    }
  );
});

//============================ Delete Portfolio By ID ===========================================//

router.delete("/:id", (req, res) => {
  const id = req.params.id;
  db.query(`DELETE FROM portfolio WHERE P_id = ${id}; `, (err, result) => {
    if (err) {
      return res.status(400).send({ msg: err });
    }

    return res.status(200).json({ msg: "Data Deleted" });
  });
});

export default router;

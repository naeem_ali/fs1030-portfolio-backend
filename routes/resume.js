import express from "express";
const router = express.Router();
import { validation, isLoggedIn } from "../Middelware/userValidation"; // Validation & Auth Middelware
import db from "../db_connection/mainConnection"; // databases

//**********************************/ Work Experiences Route /**********************************//

//================= Get Work Experiences ====================//
router.get("/", isLoggedIn, (req, res) => {
  db.query(`SELECT * FROM work`, (err, result) => {
    if (err) {
      return res.status(400).send({ msg: err });
    }

    return res.status(200).json(result);
  });
});

//================= Add Work Experiences ====================//

router.post("/", (req, res) => {
  db.query(
    `INSERT INTO work(company, position, start_date, end_date, description) VALUES(?, ?, ?, ?, ?);`,
    [
      `${req.body.company}`,
      `${req.body.position}`,
      `${req.body.start_date}`,
      `${req.body.end_date}`,
      `${req.body.description}`,
    ],
    (err, result) => {
      if (err) {
        return res.status(400).send({ msg: err });
      }

      return res.status(200).json({ msg: "Inserted New Work Experience" });
    }
  );
});

//================= Get Work Experiences By ID ====================//
router.get("/:id", (req, res) => {
  db.query(`SELECT * FROM work WHERE W_id = ${id}`, (err, result) => {
    if (err) {
      return res.status(400).send({ msg: err });
    }

    return res.status(200).json(result);
  });
});

//================= Edit Work Experiences By ID ====================//

router.put("/:id", (req, res) => {
  db.query(
    `UPDATE work SET W_id  = '${req.body.W_id}', company = '${req.body.company}', position = '${req.body.position}', start_date = '${req.body.start_date}', end_date = '${req.body.end_date}', description = '${req.body.description}' WHERE W_id  = ${id};`,
    (err, result) => {
      if (err) {
        return res.status(400).send({ msg: err });
      }

      return res.status(200).json({ msg: "Data Updated" });
    }
  );
});

//================= Delete Work Experiences By ID ====================//

router.delete("/:id", (req, res) => {
  const id = req.params.id;
  db.query(`DELETE FROM work WHERE W_id = ${id}; `, (err, result) => {
    if (err) {
      return res.status(400).send({ msg: err });
    }

    return res.status(200).json({ msg: "Data Deleted" });
  });
});

//**********************************/ Education Route /**********************************//

//================= Get Education  ====================//
router.get("/", (req, res) => {});

//================= Add Education ====================//

router.post("/", (req, res) => {});

//================= Get Education By ID ====================//
router.get("/:id", (req, res) => {});

//================= Edit Education By ID ====================//

router.put("/:id", (req, res) => {});

//================= Delete Education By ID ====================//

router.delete("/:id", (req, res) => {});

//**********************************/ Skills Route /**********************************//

//================= Get Skills  ====================//
router.get("/", (req, res) => {});

//================= Add Skill ====================//

router.post("/", (req, res) => {});

//================= Get Skill By ID ====================//
router.get("/:id", (req, res) => {});

//================= Edit Skill By ID ====================//

router.put("/:id", (req, res) => {});

//================= Delete Skill By ID ====================//

router.delete("/:id", (req, res) => {});

export default router;

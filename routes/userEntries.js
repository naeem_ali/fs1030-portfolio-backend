import express from "express";
const router = express.Router();
import { validation, isLoggedIn } from "../Middelware/userValidation"; // Validation & Auth Middelware
import db from "../db_connection/mainConnection"; // databases

//============================ View All User Entries ===========================================//
router.get("/", isLoggedIn, (req, res) => {
  // User Entries Data //
  db.query(`SELECT * FROM users_entries`, (err, result) => {
    if (err) {
      return res.status(400).send({ msg: err });
    }

    return res.status(200).json(result);
  });
});

//============================ User fill New Entry ===========================================//

router.post("/",  (req, res) => {
  // User Entries Data //
  db.query(
    `INSERT INTO users_entries( name, email, message, entry_date)VALUE( ?, ?, ?, now() )`,
    [`${req.body.name}`, `${req.body.email}`, `${req.body.message}`],
    (err, result) => {
      if (err) {
        return res.status(400).send({ msg: err });
      }

      return res.status(200).json({ msg: "Data Inserted" });
    }
  );
});

//============================ Get User Entry by ID ===========================================//

router.get("/:id", isLoggedIn, (req, res) => {
  // User Entries Data //

  const id = req.params.id;

  db.query(`SELECT * FROM users_entries WHERE U_id = ${id}`, (err, result) => {
    if (err) {
      return res.status(400).send({ msg: err });
    }

    return res.status(200).json(result);
  });
});

//============================ Edit User Entry by ID ===========================================//

router.put("/:id",  (req, res) => {
  const id = req.params.id;

  db.query(
    `UPDATE users_entries SET U_id  = '${req.body.U_id}', name = '${req.body.name}', email = '${req.body.email}', message = '${req.body.message}' WHERE U_id  = ${id};`,
    (err, result) => {
      if (err) {
        return res.status(400).send({ msg: err });
      }

      return res.status(200).json({ msg: "Data Updated" });
    }
  );
});

//============================ Delete User Entry by ID ===========================================//

router.delete("/:id",  (req, res) => {
  const id = req.params.id;
  db.query(`DELETE FROM users_entries WHERE U_id = ${id}; `, (err, result) => {
    if (err) {
      return res.status(400).send({ msg: err });
    }

    return res.status(200).json({ msg: "Data Deleted" });
  });
});

export default router;
